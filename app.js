"use strict";
var app = new Vue({
	el: "#app",
	data: {
		step: "start",
		progress: {
			current: 1,
			max: 7,
		},
		order: {
			customer: {
				billing_address: undefined
			},
			assignments: []
		},
		service_address: {
			name1: "Max",
			name2: "Mustermann",
			company_name: "Muster Corp",
			address: "Banana 11/7",
			postal_code: "",
			city: "KL",
			phone_number: "213123123",
			email: "test@example.com"
		},
		categories: [],
		devices: [],
		services: [],
		channels: [],
		regions: [],
		appointments: [],
		variant_choices: [],
		selected: {
			service_ids: [],
			subject_id: [],
			region: undefined,
			subregion: undefined,
			channel: undefined,
			variant: undefined,
			appointment: undefined
		},
		status: "",
		state: { loading: true },
		config: {
			baseurl: "http://localhost:8000/api"
		}
	},
	methods: {
		init: function() {
			this.fetch_categories();
		},
		param_string: function(params) {
			var encodeComp = (key, value) => {
				return encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
			};
			if (params === undefined) {
				return "";
			}
			var ps = "?";
			Object.keys(params).forEach(function(key, index) {
				if (params[key] instanceof Array) {
					for (var i = 0; i < params[key].length; i++) {
						ps += encodeComp(key, params[key][i]);
					}
				} else {
					ps += encodeComp(key, params[key]);
				}
			});
			return ps.substring(0, ps.length-1);
		},
		headers: function() {
			return {
				"X-Booking-Client": "ob-1"
			};
		},
		get: function(path, params) {
			return fetch(this.config.baseurl+path+this.param_string(params), {
					headers: this.headers()
				}).then(function(response) {
					return response.json();
				})
				.then(function(obj) {
					return obj;
				});
		},
		post: function(path, params) {
			return fetch(this.config.baseurl+path, {
				method: 'POST',
				headers: Object.assign(this.headers(), {"Content-Type": "application/json"}),
				body: JSON.stringify(params)
			}).then(function(response) { return response.json(); }).then(function(obj){ return obj; });
		},
		obj_by_id: function(list, id) {
			for (var i = 0; i < list.length; i++) {
				if (list[i].id == id) {
					return list[i];
				}
			}
		},
		fetch_categories: function() {
			this.get("/subjects/categories/").then((cats) => {
				this.categories = cats;
				this.state.loading = false;
			});
		},
		category_selected: function(category_id) {
			for (var c in this.categories) {
				if (this.categories[c].id == category_id) {
					this.devices = this.categories[c].subjects;
				}
			}
			this.step = 'device';
			this.progress.current++;
		},
		device_selected: function(device_id) {
			this.fetch_services(device_id);
			this.selected.subject_id = device_id;
			this.step = 'service';
			this.progress.current++;
		},
		fetch_services: function(device_id) {
			this.get("/subjects/"+device_id+"/").then((subj) => {
				this.services = subj.services;
			});
		},
		is_service_selected: function(id) {
			for (var i = 0; i < this.selected.service_ids.length; i++) {
				if (this.selected.service_ids[i] == id) {
					return true;
				}
			}
			return false;
		},
		services_selected: function() {
			for (var i = 0; i < this.selected.service_ids.length; i++) {
				var service = this.obj_by_id(this.services, this.selected.service_ids[i]);
				if (service.variant_choices && service.variant_choices.length !== 0) {
					this.variant_choices = service.variant_choices;
				}
			}
			if (this.variant_choices.length != 0) {
				this.step = 'variant';
			} else {
				this.step = 'region';
			}
			this.progress.current++;
			this.fetch_regions();
		},
		variant_selected: function(variant_id) {
			this.selected.variant = variant_id;
			this.step = 'region';
		},
		fetch_regions: function() {
			this.get("/regions/").then((regions) => {
				this.regions = regions;
			});
		},
		fetch_channels: function() {
			this.get("/channels/available/", {region_id: this.selected.region, service_id: this.selected.service_ids}).then(channels => {
				this.channels = channels;
			});
		},
		region_selected: function(region) {
			this.selected.region = region;
		},
		subregion_selected: function(subregion) {
			this.selected.subregion = subregion;
			this.fetch_channels();
			this.step = 'channel';
			this.progress.current++;
		},
		channel_selected: function(channel_id) {
			this.selected.channel = this.obj_by_id(this.channels, channel_id);
			this.progress.current++;
			if (this.selected.channel.needs_appointment) {
				this.step = 'appointment';
				this.fetch_appointments();
				return;
			}
			this.step = 'address';
		},
		fetch_appointments: function() {
			var req = {assignments: []};
			for (var i in this.selected.service_ids) {
				var srv = this.obj_by_id(this.services, this.selected.service_ids[i]);
				req.assignments.push({
					subregion: this.selected.subregion,
					subject: this.selected.subject_id,
					service: srv.id,
					channel: this.selected.channel.id,
					variant: this.selected.variant,
				});
			}
			this.post("/orders/appointments/", req).then((appointments) => {
				this.appointments = appointments;
			});
		},
		appointment_selected: function(appointment) {
			this.step = 'address';
			this.selected.appointment = appointment;
		},
		address_filled: function() {
			this.step = 'review';
			this.progress.current++;
		},
		submit: function() {
			this.step = null;
			this.status = "sending";

			if (this.order.customer.billing_address === undefined) {
				this.order.customer.billing_address = this.service_address;
			}

			for (var i in this.selected.service_ids) {
				var srv = this.obj_by_id(this.services, this.selected.service_ids[i]);
				var assgn = {
					subregion: this.selected.subregion,
					subject: this.selected.subject_id,
					service: srv.id,
					channel: this.selected.channel.id,
					variant: this.selected.variant,
					service_address: this.service_address,
				};
				if (this.selected.appointment !== undefined) {
					assgn.appointment = this.selected.appointment;
				}
				this.order.assignments.push(assgn);
			}
			this.post("/orders/", this.order).then((resp) => { this.status = 'completed'; });
		}
	},
	mounted: function() {
		this.init();
	}
});
